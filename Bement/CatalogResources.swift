//
//  CatalogResources.swift
//  Bement
//
//  Created by Runkai Zhang on 9/24/18.
//  Copyright © 2018 Numeric Design. All rights reserved.
//

import Foundation
import UIKit

struct catalog {
    
    public static let Kindergarden = [UIImage(named: "Britannica.png")!, UIImage(named: "codeorg.png")!, UIImage(named: "Core5.jpg")!, UIImage(named: "country-report.png")!, UIImage(named: "DK_Findout.png")!, UIImage(named: "infoBits.png"), UIImage(named: "KWT.png")!, UIImage(named: "thinkCentral.png")!, UIImage(named: "Fact_Monster.png")!, UIImage(named: "wonderoplis.png")!, UIImage(named: "pebblego.png")!]
    public static let Grade1 = [UIImage]()
    public static let Grade2 = [UIImage]()
    public static let Grade3 = [UIImage]()
    public static let Grade4 = [UIImage]()
    public static let Grade5 = [UIImage]()
    public static let Grade6 = [UIImage]()
    public static let Grade7 = [UIImage]()
    public static let Grade89 = [UIImage]()
}
