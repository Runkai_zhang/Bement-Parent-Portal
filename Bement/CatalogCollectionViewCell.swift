//
//  CatalogCollectionViewCell.swift
//  Bement
//
//  Created by Runkai Zhang on 9/27/18.
//  Copyright © 2018 Numeric Design. All rights reserved.
//

import UIKit

class CatalogCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var image: UIImageView!
}
